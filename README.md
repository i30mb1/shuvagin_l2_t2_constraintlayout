# Constraint Layout
Min API level is set to [`21`](https://android-arsenal.com/api?level=21)
## Tech-stack
* [Constraint layout](https://developer.android.com/reference/android/support/constraint/ConstraintLayout) - A ConstraintLayout is a android.view.ViewGroup which allows you to position and size widgets in a flexible way.

![image](screen1.png)