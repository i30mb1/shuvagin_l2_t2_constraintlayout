package com.example.shuvagin_l2_t2

import android.annotation.SuppressLint
import io.reactivex.*
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject
import io.reactivex.subjects.Subject
import java.util.concurrent.TimeUnit


@SuppressLint("CheckResult")
fun main() {
    Observable.just("one", "two")
        .map { t -> "$t 3" }
        .subscribe { t -> println(t) }

    Observable.create(ObservableOnSubscribe<Int> { it.onNext(5) })
        .subscribe { println(it) }

        //операторы создания Observable
    Flowable.just("one")
    Single.create<String> { it.onSuccess("true") }
    Observable.defer { Observable.just("5") }
    Observable.fromArray("4")
    Observable.interval(1, TimeUnit.DAYS)
    Observable.range(5, 56)
    Observable.timer(1, TimeUnit.NANOSECONDS)
    val just = Observable.just(1)

    //типы Observable
    Single.create(SingleOnSubscribe<Int> { it.onSuccess(1);it.onSuccess(4); })
        .map { "Single $it" }
        .subscribe({ it -> println(it) })
    Maybe.create(MaybeOnSubscribe<String> { it.onSuccess("5");it.onSuccess("56") })
        .map { "Maybe $it" }
        .subscribe({ it -> println(it) })
    Completable.create(CompletableOnSubscribe { it.onComplete() })
        .subscribe({ println("complete") })
    Flowable.create(FlowableOnSubscribe<Int> { }, BackpressureStrategy.BUFFER)
        .subscribe() { t: Int? -> t }

        // виды расширений Observable
    val subject = PublishSubject.create<Int>()
    subject.onNext(4)

    ReplaySubject.create<Int>()


}