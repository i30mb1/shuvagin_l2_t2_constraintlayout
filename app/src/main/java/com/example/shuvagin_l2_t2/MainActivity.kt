package com.example.shuvagin_l2_t2

import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initToolbar()
        initRecyclerView()

    }

    private fun initRecyclerView() {
        val adapter = ItemAdapter()
        rv_activity_main?.apply {
            setAdapter(adapter)
            layoutManager = GridLayoutManager(context, 1)
            addItemDecoration(DividerItemDecoration(context,LinearLayoutManager.VERTICAL))
        }
    }

    private fun initToolbar() {
        setSupportActionBar(tb_activity_main)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.hum)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }
}
