package com.example.shuvagin_l2_t2

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shuvagin_l2_t2.databinding.PersonBinding


data class Person(val name: String, val age: Int)

class ItemAdapter : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    private var inflater: LayoutInflater? = null
    private val list = mutableListOf<Person>().apply {
        (0..20).forEach {
            add(it, Person("name_$it", (18..50).shuffled().first()))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (inflater == null) inflater = LayoutInflater.from(parent.context)
        val personBinding = PersonBinding.inflate(inflater!!, parent, false)
        return ViewHolder(personBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setPerson(list[position])
    }

    class ViewHolder(private val person: PersonBinding) : RecyclerView.ViewHolder(person.root) {

        fun setPerson(item: Person) {
            person.model = item
            person.hasPendingBindings()
            person.imagePerson.rotation = (item.age *30).toFloat()
        }
    }
}
